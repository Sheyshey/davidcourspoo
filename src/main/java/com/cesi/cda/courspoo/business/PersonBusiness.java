package com.cesi.cda.courspoo.business;

import com.cesi.cda.courspoo.DAO.Person;
import com.cesi.cda.courspoo.DAO.models.PersonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.util.List;

@Component
public class PersonBusiness {

    private Person person;

    @Autowired
    public PersonBusiness(Person person) {
        this.person = person;
    }

    public List<PersonDTO> getAllPersons() throws SQLException {
        return person.getAllPersons();
    }


}
