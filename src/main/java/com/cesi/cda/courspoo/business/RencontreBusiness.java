package com.cesi.cda.courspoo.business;

import com.cesi.cda.courspoo.DAO.Rencontre;
import com.cesi.cda.courspoo.DAO.models.RencontreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.util.List;

@Component
public class RencontreBusiness {

    private Rencontre rencontre;

    @Autowired
    public RencontreBusiness(Rencontre rencontre) {
        this.rencontre = rencontre;
    }

    public List<RencontreDTO> getAllRencontres() throws SQLException {
        return rencontre.getAllRencontres();
    }

    public void deleteRencontre(Long id) throws SQLException {
        rencontre.deleteRencontre(id);
    }

    public List<RencontreDTO> postRencontre(RencontreDTO rencontreDTO) throws SQLException {
        rencontre.postRencontre(rencontreDTO);
        return rencontre.getAllRencontres();
    }
}


