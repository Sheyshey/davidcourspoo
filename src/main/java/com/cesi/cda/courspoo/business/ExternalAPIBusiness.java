package com.cesi.cda.courspoo.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.MediaType;

@Component
public class ExternalAPIBusiness {


    private final RestTemplate restTemplate;

    @Autowired
    public ExternalAPIBusiness(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String fetchDataFromExternalAPI() {
        String apiUrl = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1/persons";
        return restTemplate.getForObject(apiUrl, String.class);
    }

    public void createPerson(String nom, String prenom, int anneeNaissance, String nationalite) {
        String apiUrl = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1/persons";

        String json = String.format("{\"nom\":\"%s\",\"prenom\":\"%s\",\"anneeNaissance\":%d,\"nationalite\":\"%s\"}",
                nom, prenom, anneeNaissance, nationalite);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);

        restTemplate.postForObject(apiUrl, requestEntity, String.class);
    }
}
