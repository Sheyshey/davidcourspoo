package com.cesi.cda.courspoo.business;

import com.cesi.cda.courspoo.DAO.Person;
import com.cesi.cda.courspoo.DAO.Rencontre;
import com.cesi.cda.courspoo.DAO.models.PersonDTO;
import com.cesi.cda.courspoo.DAO.models.RencontreDTO;
import com.cesi.cda.courspoo.controller.match.models.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Component
public class MatchBusiness {

    private Person person;
    private Rencontre rencontre;

    @Autowired
    public MatchBusiness(Person person, Rencontre rencontre) {
        this.person = person;
        this.rencontre = rencontre;
    }

    public List<PersonDTO> getAllPersons() throws SQLException {
        return person.getAllPersons();
    }

    public List<RencontreDTO> getAllRencontres() throws SQLException {
        return rencontre.getAllRencontres();
    }

    public Match getMatch() throws SQLException {
        List<PersonDTO> persons = getAllPersons();
        List<RencontreDTO> rencontres = getAllRencontres();

        if (rencontres.isEmpty()) {
            throw new IllegalStateException("Aucune rencontre disponible pour créer un match.");
        }

        RencontreDTO rencontre = rencontres.get(0);
        int idGagnant = rencontre.getNuGagnant();
        int idPerdant = rencontre.getNuPerdant();

        // Récupérer uniquement les PersonDTO associés à cette première rencontre
        List<PersonDTO> personsInFirstRencontre = new ArrayList<>();
        for (PersonDTO person : persons) {
            if (person.getId() == idGagnant || person.getId() == idPerdant) {
                personsInFirstRencontre.add(person);
            }
        }

        String nomPrenomGagnant = findPersonNameById(personsInFirstRencontre, idGagnant);
        String nomPrenomPerdant = findPersonNameById(personsInFirstRencontre, idPerdant);

        String dateLieuRencontre = getAnneeTournoi(rencontre);
        String sommeAges = String.valueOf(calculateTotalAge(personsInFirstRencontre));

        return new Match(nomPrenomGagnant, nomPrenomPerdant, dateLieuRencontre, sommeAges);
    }

    public Match getMatchById(Long id) throws SQLException {
        List<RencontreDTO> rencontres = getAllRencontres();

        for (RencontreDTO rencontre : rencontres) {
            if (rencontre.getId() == id) {
                // Trouvé la rencontre, extraire les détails nécessaires
                int idGagnant = rencontre.getNuGagnant();
                int idPerdant = rencontre.getNuPerdant();

                // Récupérer uniquement les PersonDTO associés à cette rencontre
                List<PersonDTO> personsInThisRencontre = new ArrayList<>();
                for (PersonDTO person : getAllPersons()) {
                    if (person.getId() == idGagnant || person.getId() == idPerdant) {
                        personsInThisRencontre.add(person);
                    }
                }

                String nomPrenomGagnant = findPersonNameById(personsInThisRencontre, idGagnant);
                String nomPrenomPerdant = findPersonNameById(personsInThisRencontre, idPerdant);

                String dateLieuRencontre = getAnneeTournoi(rencontre);
                String sommeAges = String.valueOf(calculateTotalAge(personsInThisRencontre));

                return new Match(nomPrenomGagnant, nomPrenomPerdant, dateLieuRencontre, sommeAges);
            }
        }

        throw new IllegalArgumentException("Aucun match trouvé avec l'ID : " + id);
    }

    public Match createMatch(int idGagnant, int idPerdant, int idRencontre) throws SQLException {
        List<PersonDTO> persons = getAllPersons();
        RencontreDTO rencontre = getRencontreById(idRencontre);

        if (rencontre == null) {
            throw new IllegalArgumentException("Rencontre non trouvée avec l'ID : " + idRencontre);
        }

        List<PersonDTO> participants = new ArrayList<>();
        for (PersonDTO person : persons) {
            if (person.getId() == idGagnant || person.getId() == idPerdant) {
                participants.add(person);
            }
        }

        String nomPrenomGagnant = findPersonNameById(participants, idGagnant);
        String nomPrenomPerdant = findPersonNameById(participants, idPerdant);
        String dateLieuRencontre = getAnneeTournoi(rencontre);
        String sommeAges = String.valueOf(calculateTotalAge(participants));

        return new Match(nomPrenomGagnant, nomPrenomPerdant, dateLieuRencontre, sommeAges);
    }

    private RencontreDTO getRencontreById(int id) throws SQLException {
        List<RencontreDTO> rencontres = getAllRencontres();
        for (RencontreDTO rencontre : rencontres) {
            if (rencontre.getId() == id) {
                return rencontre;
            }
        }
        return null;
    }

    // Méthode pour obtenir la représentation de l'année du tournoi
    private String getAnneeTournoi(RencontreDTO rencontre) {
        return rencontre.getLieuTournoi() + " - " + rencontre.getAnnee();
    }

    private int calculateTotalAge(List<PersonDTO> persons) {
        int totalAge = 0;
        LocalDate currentDate = LocalDate.now();

        for (PersonDTO person : persons) {
            int birthYear = Integer.parseInt(person.getAnneeNaissance());
            int age = Period.between(LocalDate.of(birthYear, 1, 1), currentDate).getYears();
            totalAge += age;
        }

        return totalAge;
    }
    // Méthode utilitaire pour trouver le nom et prénom d'une personne à partir de son ID
    private String findPersonNameById(List<PersonDTO> persons, int id) {
        for (PersonDTO person : persons) {
            if (person.getId() == id) {
                return person.getNom() + " " + person.getPrenom();
            }
        }
        return "Nom non trouvé";
    }


}
