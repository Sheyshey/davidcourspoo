package com.cesi.cda.courspoo.controller.match.controller;

import com.cesi.cda.courspoo.business.MatchBusiness;
import com.cesi.cda.courspoo.controller.match.models.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/match")
public class MatchController {

    public MatchBusiness matchBusiness;

    @Autowired
    public MatchController(MatchBusiness matchBusiness) {
        this.matchBusiness = matchBusiness;
    }

    /* ---------------------------------- GET --------------------------------------- */
    @GetMapping()
    public Match getMatch() throws SQLException {
        return matchBusiness.getMatch();
    }

    @GetMapping("/{id}")
    public Match getMatch(@PathVariable Long id) throws SQLException {
        return matchBusiness.getMatchById(id);
    }

    /* ---------------------------------- POST --------------------------------------- */
    @PostMapping()
    public ResponseEntity<?> postMatch(@RequestParam int idGagnant, @RequestParam int idPerdant, @RequestParam int idRencontre) {
        try {
            Match match = matchBusiness.createMatch(idGagnant, idPerdant, idRencontre);
            return ResponseEntity.ok(match);
        } catch (SQLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de la création du match : " + e.getMessage());
        }
    }

    /* ---------------------------------- PUT --------------------------------------- */
    @PutMapping("/{id}")
    public String updateMatch(@PathVariable Long id) {
        return "match modifier";
    }

    /* ---------------------------------- DELETE --------------------------------------- */
    @DeleteMapping("/{id}")
    public String deleteMatch(@PathVariable Long id) {
        return "match delete";
    }

}
