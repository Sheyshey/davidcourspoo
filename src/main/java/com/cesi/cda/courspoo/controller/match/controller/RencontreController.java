package com.cesi.cda.courspoo.controller.match.controller;
import com.cesi.cda.courspoo.DAO.models.RencontreDTO;
import com.cesi.cda.courspoo.business.RencontreBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
public class RencontreController {

    public RencontreBusiness rencontreBusiness;

    @Autowired
    public RencontreController(RencontreBusiness rencontreBusiness) {
        this.rencontreBusiness = rencontreBusiness;
    }

    /* ---------------------------------- GET --------------------------------------- */

    @GetMapping("/rencontres")
    public List<RencontreDTO> getRencontres() throws SQLException {
        return rencontreBusiness.getAllRencontres();
    }

    /* ---------------------------------- POST --------------------------------------- */

    @PostMapping("/createRencontre")
    public List<RencontreDTO> postRencontre(@RequestBody RencontreDTO rencontreDTO) throws SQLException {
        return rencontreBusiness.postRencontre(rencontreDTO);
    }

    /* ---------------------------------- DELETE --------------------------------------- */
    @DeleteMapping("/rencontres/{id}")
    public ResponseEntity<String> deleteRencontre(@PathVariable Long id) {
        try {
            rencontreBusiness.deleteRencontre(id);
            return ResponseEntity.ok("Rencontre supprimé avec succès");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur lors de la suppression du match: " + e.getMessage());
        }
    }

}
