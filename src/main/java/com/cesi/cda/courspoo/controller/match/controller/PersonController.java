package com.cesi.cda.courspoo.controller.match.controller;

import com.cesi.cda.courspoo.DAO.models.PersonDTO;
import com.cesi.cda.courspoo.business.ExternalAPIBusiness;
import com.cesi.cda.courspoo.business.PersonBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
public class PersonController {

    private final ExternalAPIBusiness externalAPIBusiness;
    public PersonBusiness personBusiness;

    @Autowired
    public PersonController(PersonBusiness personBusiness, ExternalAPIBusiness externalAPIBusiness) {
        this.personBusiness = personBusiness;
        this.externalAPIBusiness = externalAPIBusiness;
    }

    /* ---------------------------------- GET --------------------------------------- */

    @GetMapping("/persons")
    public List<PersonDTO> getPersons() throws SQLException {
        return personBusiness.getAllPersons();
    }

    @GetMapping("/personsFromAPI")
    public String getDataFromExternalAPI() throws SQLException {
        return externalAPIBusiness.fetchDataFromExternalAPI();
    }

    /* ---------------------------------- POST --------------------------------------- */
    @PostMapping("/personsFromAPI")
    public ResponseEntity<String> createPerson(@RequestBody PersonDTO personDTO) {
        try {
            externalAPIBusiness.createPerson(personDTO.getNom(), personDTO.getPrenom(),
                    Integer.parseInt(personDTO.getAnneeNaissance()), personDTO.getNationalite());
            return ResponseEntity.ok("Personne créée avec succès");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Erreur lors de la création de la personne : " + e.getMessage());
        }
    }

}
